include Math


class OSMap

  attr_accessor :roads, :bounds

  # Returns GrahpViz object
  # If instance variable doesn't hold one, it attempts to create it.

  def graph(opts = [])
    @graph ||= make_graph(opts: opts)
  end

  def initialize(minlon: 0, minlat: 0, maxlon: 0, maxlat: 0)
    @bounds, @roads = {}, {}
    set_bounds(minlat: minlon, minlon: minlat, maxlat: maxlon, maxlon: maxlat)
  end

  def set_bounds(minlon: 0, minlat: 0, maxlon: 0, maxlat: 0)
    @bounds[:minlon], @bounds[:minlat], @bounds[:maxlon], @bounds[:maxlat] = minlon, minlat, maxlon, maxlat
  end

  def distance(lon1:, lat1:, lon2:, lat2:)
    earth_radius = 6371
    r_lon1, r_lat1 = lon1 * PI / 180, lat1 * PI / 180
    r_lon2, r_lat2 = lon2 * PI / 180, lat2 * PI / 180
    earth_radius * 2 * asin(sqrt(sin((r_lat2-r_lat1)/2)**2 + cos(r_lat1) * cos(r_lat2) * sin((r_lon2 - r_lon1)/2)**2))
  end


  # Processes nodes from list of roads and constructs GraphViz graph object

  def make_graph(opts: [])
    @graph = GraphViz.digraph('Map',
                              URL: 'http://www.openstreetmap.org/export#map=16/50.1010/14.3966',
                              truecolor: true,
                              inputscale: 0.0001,
                              size: '11.6929,8.2677!',
                              margin: 0,
                              use: :neato,
                              bb: "#{bounds[:minlon]},#{bounds[:minlat]},
                                  #{bounds[:maxlon]},#{bounds[:maxlat]}",
                              outputorder: :nodesfirst) do |gv_graph|

      roads.each_pair do |road_id, node_map|
        nodes_edge = []
        it = node_map[:nodes].each_pair do |node_id, pos|
          gv_graph.add_nodes(node_id.to_s, pos: "#{pos[:lon]}, #{pos[:lat]}",
                             shape: opts.include?('--show-nodes') ? :circle : :point,
                             pin: true,
                             label: node_id.to_s)
          nodes_edge << node_id
        end
        nodes_edge.each_cons(2) do |e|
          gv_graph.add_edge(e[0].to_s, e[1].to_s,
                            penwidth: 15,
                            arrowsize: node_map[:attrs][:oneway] ? 1 : 0,
                            dir: node_map[:attrs][:oneway] ? :forward : :both,
                            comment: node_map[:attrs][:maxspeed],
                            weight: distance(lon1: node_map[:nodes][e[0]][:lon],
                                             lat1: node_map[:nodes][e[0]][:lat],
                                             lon2: node_map[:nodes][e[1]][:lon],
                                             lat2: node_map[:nodes][e[1]][:lat],
                            )
          )
        end
      end
    end
    @graph
  end

  def add_overlay(edge_list:, color: '#00ff00af', options: {})
    options.each do |node, opts|
      @graph.add_node(node, opts)
    end
    edge_list.each do |e|
      @graph.add_edge(e.from, e.to,
                      arrowsize: 0,
                      penwidth: 50,
                      color: color,
                      constraint: false)
    end
  end

end