# luhačovice http://www.openstreetmap.org/export#map=19/49.09992/17.75723
# dejvice http://www.openstreetmap.org/export#map=17/50.10051/14.39537


require 'pp'
require 'graphviz'

require_relative 'lib/os_map'
require_relative 'lib/sax_handler'

require_relative 'lib/edge_weighted_graph'
require_relative 'lib/edge_weighted_digraph'
require_relative 'lib/dijkstra_sp'
require_relative 'lib/jarnik_mst'

class Osm2Graphviz


  def initialize
    @usage_text = <<-END.gsub(/^ {6}/, '')
      Usage:\truby osm2graphviz.rb --load    <map.osm> <output.pdf>
      \t\truby osm2graphviz.rb --mst     <map.osm> <source_node>
      \t\truby osm2graphviz.rb --mindist <map.osm> <source_node> <destination_node> <output.pdf> [--show_nodes]
    END
  end

  def usage
    puts @usage_text
  end

  # TODO: robust command line handling
  def process_args
    unless ARGV.length >= 3
      puts usage
      exit 1
    end
    @operation = ARGV.shift
    @map_file = ARGV.shift
    if @operation == '--mindist'
      @source_node = ARGV.shift
      @destination_node = ARGV.shift
    end
    if @operation == '--mst'
      @source_node = ARGV.shift
    end
    @out_file = ARGV.shift
    @opts = ARGV
  end

  def osm2gv(osm_xml)
    os_map = OSMap.new
    handler = SaxHandler.new(os_map)

    File.open(osm_xml, 'r') do |f|
      Ox.sax_parse(handler, f, symbolize: true)
    end
    os_map
  end

  def export_graph(graph:, format: :pdf, file_name: 'output')
    /.(?<extension>[a-z0-9]*$)/ =~ file_name
    file_name = file_name + '.' + format.to_s unless extension == format
    graph.output(format.to_sym => file_name)
  end

  def path_and_distance(graph:, source:, destination:)
    ewg = EdgeWeightedDigraph.new graph.node_count

    graph.each_edge do |e|
      ewg.add_edge DirectedEdge.new(from: e.node_one(false, false), to: e.node_two(false, false), weight: e[:weight].to_f)
      ewg.add_edge DirectedEdge.new(from: e.node_two(false, false), to: e.node_one(false, false), weight: e[:weight].to_f) if e[:dir].to_ruby == 'both'
    end

    shortest_path = DijkstraSP.new(graph: ewg, start: source)

    return shortest_path.path_to(destination), shortest_path.dist_to(destination)
  end

  def trip_time(path: p)
    path_edges = []
    total_time = 0

    path.each_with_object(path_edges) do |p, edges|
      @os_map.graph.each_edge do |e|
        if p.from == e.node_one(false, false) && p.to == e.node_two(false, false)
          edges << e
        end
      end
    end

    path_edges.each do |e|
      total_time += (e[:weight].to_f / e[:comment].to_ruby.to_f)
    end
    return total_time
  end

  def mst(graph:, node:)
    # TODO: handle not necessarily connected graphs
    # TODO: path compression
    ewg = EdgeWeightedGraph.new graph.node_count

    graph.each_edge do |e|
      ewg.add_edge Edge.new(v: e.node_one(false, false), w: e.node_two(false, false), weight: e[:weight].to_f)
    end

    prim = JarnikMST.new(graph: ewg, start: node)
    prim.mst
  end

  def run
    process_args
    @os_map = osm2gv @map_file
    graph_g = @os_map.graph @opts
    case @operation
      when '--load'
        export_graph(graph: graph_g, format: :pdf, file_name: @out_file)
        return
      when '--mindist'
        path, dist = path_and_distance(graph: graph_g, source: @source_node, destination: @destination_node)
        # TODO: fix this annoying issue
        # above line cannot stand after add_overlay method as it cannot handle nodes which add_overlay adds to a graph
        if path.nil?
          puts 'Connection does not exist.'
          exit 2
        elsif @opts.include?('--print-path')
          puts path
        end
        tt = trip_time path: path
        print '%.2f' % dist + ' km za ' + '%.2f' % tt + ' hodin'
        @os_map.add_overlay(edge_list: path,
                            options: {path.first.to =>
                                          {shape: :circle, penwidth: 25, color: '#ff0000af', label: 'cíl',
                                           fontsize: 50},
                                      path.last.from =>
                                          {shape: :circle, penwidth: 25, color: '#00ff00af', label: 'start',
                                           fontsize: 50}})
        export_graph(graph: graph_g, format: :pdf, file_name: @out_file)
        return
      when '--mst'
        mst_path = mst(graph: graph_g, node: @source_node)
        @os_map.add_overlay(edge_list: mst_path,
                            color: '#00ffffaf')
        export_graph(graph: graph_g, format: :pdf, file_name: @out_file)
        return
      else
        usage
        exit 1
    end
  end

end


osm2gv = Osm2Graphviz.new
osm2gv.run