# Zadání

Úkolem tohoto projektu je vytvořit jednoduchou navigaci, která načte data ze systému [OpenStreetMap](http://www.openstreetmap.org), bude umět vyhledat nejkratší cestu mezi cílovými body a výsledek zobrazit v systému [Graphviz](http://www.graphviz.org).

Silnice pro motorová vozidla jsou popsány elementy `<way>` s vnořeným elementem `<tag>` s atributem `k="highway" v=hodnota`, kde hodnotou může být

 * motorway,
 * trunk,
 * primary,
 * secondary,
 * tertiary,
 * unclassified,
 * residential,
 * service.
---
# Instalace
Použijte [Bundler](http://bundler.io).

`bundle install`

# Použití
## Vykreslení mapy
`ruby osm2graphviz.rb --load <map.osm.xml> <output.pdf> [--show-nodes]`
Pokud jsou ve vstupních datech obsaženy jednosměrné ulice, jsou zobrazeny i ve výsledné mapě.

Při spuštění s parametrem `--show-nodes` budou ve výsledném pdf dokumentu zobrazeny jednotlivé body mapy spolu s jejich číselnými označeními. Tyto slouží k zadání počátku a cíle cesty při vyhledávání nejkratšího spojení. 
 
## Vyhledání nejkratší cesty
`ruby osm2graphviz.rb --mindist <map.osm.xml> <source_node> <destination_node> <output.pdf> [--show-nodes] [--print-path]`

## Nalezení minimální kostry
 `ruby osm2graphviz.rb --mst <map.osm.xml> <source_node> <output.pdf> [--show-nodes]`
 
### Příklad použití
Vyhledání cesty od stanice metra _Hradčanská_ do _Národní technické knihovny_ s využitím dat [příslušné oblasti](http://www.openstreetmap.org/export#map=17/50.10194/14.39480).
 
```
$ ruby osm2graphviz.rb --mindist dejvice.osm.xml 25692585 331441553 mapa.pdf
 
1.47 km za 0.02 hodin
```
![alt text](mapa.png)
