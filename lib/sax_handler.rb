require 'ox'

class SaxHandler < ::Ox::Sax
  MAP_TAGS = [:bounds, :node, :way, :nd, :tag]
  BOUNDS_ATTRS = [:minlat, :minlon, :maxlat, :maxlon]
  NODE_ATTRS = [:lat, :lon, :type, :id]
  WAY_ATTRS = [:id]
  REAL_WAY_ATTRS = %w(motorway trunk primary secondary tertiary unclassified residential service bus_stop crossing)
  COMBINED_ATTRS = BOUNDS_ATTRS + WAY_ATTRS
  ATTR_MAP = {lat: :to_f, lon: :to_f, id: :to_i, minlat: :to_f, minlon: :to_f, maxlat: :to_f, maxlon: :to_f}

  def initialize(os_map)
    @nodes = {}
    @ways = []
    @attrs = {}
    @os_map = os_map
    @highway = false
    @real_way = false
  end

  def start_element(name)
    return unless MAP_TAGS.include?(name)
    @current_element = name
    case name
      when :bounds
        @bounds = {}
      when :node
        @current_node = {}
      when :way
        @current_way = {}
        @current_way_node_list = {}
        @real_way = false
        @highway = false
        @inside_way_tag = true
        @oneway = false
        @maxspeed = 50
    end
  end

  def attr(name, str)
    case @current_element
      when :bounds
        @bounds[name] = str.send(ATTR_MAP[name])
      when :node
        return unless NODE_ATTRS.include?(name)
        @current_node[name] = str.send(ATTR_MAP[name])
      when :way
        return unless WAY_ATTRS.include?(name)
        @current_way[name] = str.send(ATTR_MAP[name])
      when :nd
        ref = str.to_i
        # is @nodes.delete() safe? If nodes are referenced in multiple way entities, should be changed to simple = assignment
        # @current_way_node_list[ref] ||= @nodes.delete(ref)
        @current_way_node_list[ref] = @nodes[ref]
      when :tag
        # only interested in tags under way element
        return unless @inside_way_tag
        @highway_tag = true if name == :k && str == 'highway'
        @real_way = true if @highway_tag && name == :v && REAL_WAY_ATTRS.include?(str)
        @oneway_tag = true if name == :k && str == 'oneway'
        @oneway = true if @oneway_tag && name == :v && str == 'yes'
        # TODO: add handling of different speed units like mph or knots
        @maxspeed_tag = true if name == :k && str == 'maxspeed'
        @maxspeed = str.to_i if @maxspeed_tag && name == :v
    end
  end

  def attrs_done
    case @current_element
      when :node
        @nodes[@current_node[:id]] = {lon: @current_node[:lon], lat: @current_node[:lat]}
      when :tag
        assign_nodes_to_way if @real_way
        assign_attrs_to_way if @real_way
        @highway_tag = false
        @oneway_tag = false
        @maxspeed_tag = false
    end
  end

  def end_element(name)
    @last_element = name
    case name
      when :bounds
        @os_map.set_bounds(minlon: @bounds[:minlon], minlat: @bounds[:minlat], maxlon: @bounds[:maxlon], maxlat: @bounds[:maxlat])
      when :way
        @inside_way_tag = false
        @real_way = false
        @oneway = false
    end
  end

  def assign_nodes_to_way
    @os_map.roads[@current_way[:id]] ||= {}
    @os_map.roads[@current_way[:id]][:nodes] = @current_way_node_list
  end

  def assign_attrs_to_way
    @os_map.roads[@current_way[:id]][:attrs] ||= {}
    @os_map.roads[@current_way[:id]][:attrs][:oneway] = @oneway
    @os_map.roads[@current_way[:id]][:attrs][:maxspeed] = @maxspeed
  end

end