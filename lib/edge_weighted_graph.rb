require_relative 'edge'

class EdgeWeightedGraph
  attr_reader :V, :E, :adjacency_list

  def initialize(number_of_vertices)
    @V, @E = number_of_vertices, 0
    @adjacency_list = Hash.new
  end

  def add_edge(edge)
    @adjacency_list[edge.v] ||= []
    @adjacency_list[edge.w] ||= []
    @adjacency_list[edge.v] << edge
    @adjacency_list[edge.w] << edge
    @E += 1
  end

  def adj(vertex)
    @adjacency_list[vertex]
  end

  def degree(vertex)
    @adjacency_list[vertex].size
  end

  def edges
    @adjacency_list.each_with_object([]) do |vertex_edges, a|
      vertex_edges.each { |edge| a << edge }
    end
  end

end