class Edge
  include Comparable
  attr_reader :v, :w, :weight
  alias_method :from, :v
  alias_method :to, :w

  def initialize(v:, w:, weight:)
    @v, @w, @weight = v, w, weight
  end

  def either
    @v
  end

  def other(vertex)
    case vertex
      when @v
        return @w
      when @w
        return @v
      else
        return nil
    end
  end

  def to_s
    "#{@v} — #{@w} " '%.2f' % @weight
  end

  def <=> other
    @weight <=> other.weight
  end
end