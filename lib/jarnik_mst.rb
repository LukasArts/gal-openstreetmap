require_relative 'min_pq'

class JarnikMST
  attr_reader :mst

  def initialize(graph:, start:)
    @marked = Hash.new(false)
    @mst = Array.new
    @pq = MinPQ.new(graph.V)

    visit(graph: graph, vertex: start)

    until @pq.empty?
      edge = @pq.del_min
      v = edge.either
      w = edge.other(v)
      next if @marked[v] && @marked[w]
      @mst << edge
      visit(graph: graph, vertex: v)
      visit(graph: graph, vertex: w)
    end
  end


  private

  def visit(graph:, vertex:)
    @marked[vertex] = true
    graph.adj(vertex).each do |edge|
      @pq.insert(edge) unless @marked[edge.other(vertex)]
    end
  end
end