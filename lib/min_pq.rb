class MinPQ

  def initialize(maxN)
    @pq = Array.new(maxN)
    @N = 0
  end

  def empty?
    @N == 0
  end

  def size
    @N
  end

  def insert(key)
    @N += 1
    @pq[@N] = key
    swim(@N)
  end

  def del_min
    exch(1, @N)
    min = @pq[@N]
    @N -= 1
    sink 1
    @pq[@N+1] = nil
    min
  end

  private

  def swim(k)
    while k > 1 && greater(k/2, k)
      exch k, k/2
      k = k/2
    end
  end

  def sink(k)
    while 2*k <= @N
      j = 2*k
      j += 1 if j < @N && greater(j, j+1)
      break unless greater(k, j)
      exch(k, j)
      k = j
    end
  end

  def greater(i, j)
    @pq[i] > @pq[j]
  end

  def exch(i, j)
    @pq[i], @pq[j] = @pq[j], @pq[i]
  end

end